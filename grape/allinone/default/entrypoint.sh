#!/bin/bash

. /usr/share/migasfree-server/setup/db_server.sh
. /usr/share/migasfree-server/setup/web_server.sh

# config cron
echo "00 00 * * * /usr/bin/backup-db" > /tmp/cron
crontab /tmp/cron 
rm /tmp/cron
service cron restart



function pg__data_init
{ 
    # Create postgres data directory and run initdb if needed
    if ! [ "$(ls -A $PG_DATA)" ]  ; then # directory data is empty
        echo "Initializing database files"
        chown postgres:postgres $PG_DATA
        cmd="/usr/lib/postgresql/$(get_pg_major_version)/bin/initdb -D $PG_DATA"
        su postgres -l -c "$cmd" || :
    fi
}

function set_listen_addresses
{
    sedEscapedValue="$(echo "$1" | sed 's/[\/&]/\\&/g')"
    sed -ri "s/^#?(listen_addresses\s*=\s*)\S+/\1'$sedEscapedValue'/" /etc/postgresql/$(get_pg_major_version)/main/postgresql.conf
}

set_listen_addresses "*"
for _ELEMENT in $PG_ALLOW_HOSTS
do 
    echo "host all all $_ELEMENT trust" >> /etc/postgresql/$(get_pg_major_version)/main/pg_hba.conf
done

pg__data_init
db_server_init
web_server_init



while :
do 
    sleep 1
done
