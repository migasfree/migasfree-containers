# migasfree-grape-allinone image

Provides an isolated migasfree server to run in one host.

This image exposes the 5432 and 80 ports (postgres and http port).


## How to use this image


### To run


```sh
FQDN="migasfree.mydomain.com" TZ="Europe/Madrid" PG_ALLOW_HOSTS="192.168.1.0/24" make run
```

This will download and start a migasfree-grape-allinone container and publish
ports needed to access HTTP and POSTGRES. All migasfree data will be stored as
subdirectories of **/var/lib/migasfree/FQDN/**.

You can now login to the web interface with:

    user: admin

    password: admin

#### Where is the data stored?

The migasfree-grape-allinone container uses host mounted volumes to store persistent data:


| Local location                  | Usage                                               |
|---------------------------------|-----------------------------------------------------|
| /var/lib/migasfree/FQDN/conf/   | For storing the settings.py file.                   |
| /var/lib/migasfree/FQDN/data/   | For storing postgres data.                          |
| /var/lib/migasfree/FQDN/keys/   | For migasfree keys.                                 |
| /var/lib/migasfree/FQDN/dump/   | For dump files used in backup and restore database. |
| /var/lib/migasfree/FQDN/public/ | For repositories and static files.                  |


In addition, all days at 00:00 a dump of database is stored in /var/lib/migasfree/FQDN/dump/migasfree.sql


### To backup database

Sample:

```sh
FQDN="migasfree.mydomain.com" DUMP_FILE="migasfree-15.sql" make backup-db
```

### To restore database

Sample:

```sh
FQDN="migasfree.mydomain.com" DUMP_FILE="migasfree-15.sql" make restore-db
```

### To create a database from scratch:

If not exists the configuration file /var/lib/migasfree/FQDN/conf/settings.py it is created.

Sample:

```sh
FQDN="migasfree.mydomain.com" make from-scratch
```

### To build

Creates the image migasfree/grape-allinone

```sh
make build
```

### To push to hub.docker.com

```sh
make push
```

### To pull from hub.docker.com

```sh
make pull
```

## Environment Variables

| Name           | Description                        | Default value            | Sample                          |
|----------------|------------------------------------|--------------------------|---------------------------------|
| FQDN           | Fully Qualified Domain Name        | 'migasfree.mydomain.com' |                                 |
| TZ             | Time Zone                          | 'Europe/Madrid'          |                                 |
| PG_ALLOW_HOSTS | Hosts list with access to postgres | ''                       | '192.168.1.0/24 192.168.2.0/24' |
| DUMP_FILE      | Dump file name                     | 'migasfree.sql'          | 'migasfree-2.sql'               |

## Links

[Image in hub.docker.com](https://hub.docker.com/r/migasfree/grape-allinone/)

[Documentation in spanish](http://fun-with-migasfree.readthedocs.org/en/master/)